const path = require('path');

const filename = mode => mode === 'development' ? 'index.html' : '200.html';

module.exports = (options, req) => ({
  entry: 'src/index.js',
  html: {
    template: 'index.html',
    filename: filename(options.mode)
  },
  transformModules: ['redom'],
  sourceMap: options.mode === 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist')
  }
});
