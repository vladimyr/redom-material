import { el } from 'redom/src';
import { propsToClasses, createElement } from './helpers';
import ButtonIcon from './ButtonIcon';

export default class Button {
  constructor({
    unelevated = true,
    raised = false,
    stroked = false,
    dense = false,
    compact = false,
    icon,
    color,
    disabled
  } = {}, { children } = {}) {
    const classes = propsToClasses({
      unelevated,
      raised,
      stroked,
      dense,
      compact
    }, this.constructor.baseClass);
    this.el = el(
      createElement(this.constructor.tagname, classes),
      icon && new ButtonIcon({ icon }),
      children
    );
    if (disabled) this.el.disabled = disabled;
    if (color) this.el.style.setProperty('--mdc-theme-primary', color);
  }
  static get tagname() { return 'button'; }
  static get baseClass() { return 'mdc-button'; }
}
