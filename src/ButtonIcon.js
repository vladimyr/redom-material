import { el} from 'redom/src';
import { propsToClasses, createElement } from './helpers';

export default class ButtonIcon {
  constructor({ icon } = {}, { children } = {}) {
    this.el = el(
      createElement(
        this.constructor.tagname,
        [this.constructor.baseClass, 'material-icons']
      ),
      children
    );
    if (icon) this.el.textContent = icon;
  }
  static get tagname() { return 'i'; }
  static get baseClass() { return 'mdc-button__icon'; }
}
