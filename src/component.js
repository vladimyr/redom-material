import { setAttr } from 'redom/src';
import { parseQuery } from 'redom/src/parse-query';
const isString = arg => typeof arg === 'string';
const isNumber = arg => typeof arg === 'number';
const isFunction = arg => typeof arg === 'function';
const isObject = arg => typeof arg === 'object';
const isNode = arg => arg && arg.nodeType;
const getEl = parent => (parent.nodeType && parent) || (!parent.el && parent) || getEl(parent.el);

const components = {};

export default function c(query, ...args) {
  if (!isString(query)) throw new Error('Invalid query');
  const { tag: selector, id, className } = parseQuery(query);
  const { props, slots, attrs, middlewares } = parseArguments(args);
  const View = components[selector];
  if (!View) return;
  const view = new View(props, slots);
  const el = getEl(view);
  if (!el) throw new Error('Component does not have element');
  if (id) el.id = id;
  if (attrs) setAttr(el, attrs);
  if (className && el.className) {
    el.className = className + ' ' + el.className;
  } else if (className) {
    el.className = className;
  }
  while (middlewares.length) {
    const fn = middlewares.shift();
    fn(el);
  }
  return view;
}

c.register = (selector, View) => components[selector] = View;

function parseArguments(args = [], res = {
  props: null,
  attrs: {},
  middlewares: [],
  slots: {
    children: []
  }
}) {
  args.forEach(arg => {
    if (isFunction(arg)) {
      res.middlewares.push(arg);
    } else if (isString(arg) || isNumber(arg)) {
      res.slots.children.push(arg);
    } else if (isNode(getEl(arg))) {
      res.slots.children.push(arg);
    } else if (arg.length) {
      parseArguments(arg, res);
    } else if (isObject(arg)) {
      if (!res.props) {
        res.props = arg;
      } else {
        Object.assign(res.attrs, arg);
      }
    }
  });
  res.props = res.props || {};
  return res;
}

