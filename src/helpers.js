import decamelize from 'decamelize';

export function createElement(tagname, classes = []) {
  const el = document.createElement(tagname);
  if (classes.length) el.className = classes.join(' ');
  return el;
}

export function propsToClasses(props, baseClass) {
  const classes = Object.keys(props).reduce((classes, name) => {
    const value = props[name];
    if (value) classes.push(`${baseClass}--${decamelize(name, '-')}`);
    return classes;
  }, []);
  classes.unshift(baseClass);
  return classes;
}
