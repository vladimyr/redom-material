import { el, mount } from 'redom';
import c from './component';

import Button from './Button';
c.register('button', Button);

const demo = el('.demo',
  el('.message', 'Much buttons, such WOW'),
  c('button.btn', 'click me', { color: '#e91e63', raised: true, compact: true, icon: 'favorite' }, { 'some-attr': false }),
  c('button.btn', 'flat button'),
  c('button.btn.much-classes', 'dense', { color: '#212121', unelevated: true, dense: true }),
  c('button.btn', 'disabled', { color: '#212121', unelevated: true, disabled: true })
);
mount(document.getElementById('app'), demo);
